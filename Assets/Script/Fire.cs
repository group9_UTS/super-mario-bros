﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{

    public float speed = 5f;
    public Vector2 velocity;

    private Rigidbody2D rb;

    float count;

    PlayerController playerController;

    // Start is called before the first frame update
    void Start() {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();

        rb = this.GetComponent<Rigidbody2D>();
        if (!playerController.isLeft) {
            rb.velocity = new Vector2(velocity.x, velocity.y);
            velocity = rb.velocity;
        } else {
            rb.velocity = new Vector2(-velocity.x, velocity.y);
            velocity = rb.velocity;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.y < velocity.y)
        {
            rb.velocity = velocity;
        }

        count += Time.deltaTime;
        if (count > 3)
        {
            Destroy();
        }

        if (rb.velocity.x == 0) //Destroys fireball when hitting vertical surface
        {
            Destroy();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        rb.velocity = new Vector2(velocity.x / 2, -velocity.y / 4);

        if (collision.collider.tag == "Enemy")
        {
            Destroy(collision.gameObject);
            Destroy();
        }
    }

    private void Destroy()
    {
        Destroy(this.gameObject);
    }
}