﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    bool isActivated = false;

    public float speed;
    float speedOriginal;

    public bool stepped = false;
    public bool unrevivable = false;
    Animator animator;
    SpriteRenderer spriteRenderer;

    public bool isDungeon;

    IEnumerator lastCoroutine;

    bool entered = false;

    enum Enemy {
        Goomba,
        Troopa,
        Flower,
        Bowser,
        none
    }

    private void Start() {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (isDungeon) animator.SetBool("IsDungeon",true);
    }

    // Update is called once per frame
    void Update() {
        if (isActivated) {
            if (unrevivable) {          //when troopa is in-shell and was thrown
                transform.position = new Vector2(transform.position.x + (speed * Time.deltaTime), transform.position.y);
            } else if (stepped) {       //when enemy was stepped from top
                switch (getEnemyType()) {
                    case Enemy.Goomba:
                        animator.SetBool("Stepped", true);
                        gameObject.GetComponent<BoxCollider2D>().enabled = false;
                        gameObject.transform.parent.Find("GameObject").gameObject.SetActive(false);
                        transform.parent.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
                        StartCoroutine(stayForASecs(1));
                        break;
                    case Enemy.Troopa:
                        animator.SetBool("Stepped", true);
                        gameObject.GetComponent<PolygonCollider2D>().enabled = false;
                        gameObject.transform.Find("InShellCollisionBox").gameObject.SetActive(true);
                        gameObject.transform.Find("PlayerStep").GetComponent<PolygonCollider2D>().enabled = false;
                        break;
                    case Enemy.Flower:
                        break;
                    case Enemy.Bowser:
                        break;
                }
            } else {                    //default AI
                float varx = transform.position.x + (speed * Time.deltaTime);
                transform.position = new Vector2(varx, transform.position.y);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        //if (collision.collider.tag != "ground") {
            speed = -speed;
            spriteRenderer.flipX = !spriteRenderer.flipX;
        if (collision.collider.tag!="MainCamera") entered = true;
        //}
    }

    private void OnTriggerExit2D(Collider2D collision) {
        entered = false;
    }

    public void SetStepped(bool value) {
        stepped = value;
        if (unrevivable) {
            unrevivable = false;
            speed = speedOriginal;
            if (speed > 0) spriteRenderer.flipX = true;
            else spriteRenderer.flipX = false;
            if (lastCoroutine != null) StopCoroutine(lastCoroutine);
            StartCoroutine(stayForASecs(3.0f));
        }
    }

    private Enemy getEnemyType() {
        string nameToTest = gameObject.name;
        if (nameToTest.Contains("Goomba")) return Enemy.Goomba;
        else if (nameToTest.Contains("Troopa")) return Enemy.Troopa;
        else if (nameToTest.Contains("Flower")) return Enemy.Flower;
        else if (nameToTest.Contains("Bowser")) return Enemy.Bowser;
        return Enemy.none;
    }

    public void doCoroutine(float length) {         //for calling coroutine in statemachinebehaviour, animations
        lastCoroutine = stayForASecs(length);
        StartCoroutine(lastCoroutine);
    }

    IEnumerator stayForASecs(float length) {
        //float temp = Time.time;
        //print("called"+temp);
        yield return new WaitForSeconds(length);
        if (getEnemyType() == Enemy.Troopa) {
            if (!unrevivable) animator.SetTrigger("Revive");
        } else {
            gameObject.SetActive(false);
            stepped = false;
        }
        //print("finished"+temp);
    }

    public void moveAsAShell(int direction) {
        unrevivable = true;
        stepped = false;
        speedOriginal = speed;
        speed = speed * 2 * direction;
    }
    public void whenMoveAsAShellChangeDirection() {
        speed = speed * -1;
    }

    public void beActivated() {
        isActivated = true;
    }
}