﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float dirX;
    private float moveSpeed = 1;
    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent < Rigidbody2D>();
        dirX = -3f;
    }

    
    void Update()
    {
        //when enemy get to this x coordinate, move right
        if (transform.position.x < 2.55f)
            dirX = 3f;
        //when enemy get to this x coordinate, move left
        else if (transform.position.x > 13.55f)
            dirX = -3f;
    }

    private void FixedUpdate()
    {
        //makes the enemy move
        rb.velocity = new Vector2(dirX * moveSpeed, rb.velocity.y);
    }
}
