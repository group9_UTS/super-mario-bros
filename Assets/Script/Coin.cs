﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour{
    private void OnTriggerEnter2D(Collider2D collision) {
        //print(collision.name);
        if (collision.transform.parent.parent.tag != null && collision.transform.parent.parent.tag == "Player") {
            GameObject.Find("SoundManager").GetComponent<SoundManager>().playSound(SoundManager.Sound.Coin);
            gameObject.SetActive(false);
        }
    }
}
