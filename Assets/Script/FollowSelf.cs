﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSelf : MonoBehaviour
{
    GameObject toFollow;
    // Start is called before the first frame update
    void Start()
    {
        toFollow = transform.parent.Find("Goomba").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = toFollow.transform.position;
        
    }
}
