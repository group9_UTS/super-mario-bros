﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
    public int lifes = 3;

    private void Start() {
        DontDestroyOnLoad(this);
    }

    public void changeScene(int pivot) {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + pivot);
        if (lifes <= 0) lifes = 3;
        //PlayerPrefs.SetInt("Lives", 3);
    }
}
