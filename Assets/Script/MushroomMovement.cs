﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomMovement : MonoBehaviour {
    public int speed;
    void Update() {
        transform.parent.position = new Vector2(transform.parent.position.x + speed*Time.deltaTime, transform.parent.position.y);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        speed = -speed;
        
        if (collision.transform.gameObject.tag == "Player") transform.parent.gameObject.SetActive(false);
    }
}