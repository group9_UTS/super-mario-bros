﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepCheck : MonoBehaviour {
    public GameObject parent;
    private void Start() {
        parent = transform.parent.gameObject;
        if (parent.name.Contains("InShell")) parent = transform.parent.parent.gameObject;
    }

    private void OnTriggerEnter2D(Collider2D collider2D) {
        print("1111");
        if (collider2D.transform.parent.gameObject.tag == "Player") {
            parent.GetComponent<EnemyController>().SetStepped(true);
        }
    }
}