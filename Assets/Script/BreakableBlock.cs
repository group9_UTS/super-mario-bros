﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableBlock : MonoBehaviour {
    public bool wasFromBottom = false;
    public float timetillbreak;
    PlayerController player;

    private void Start() {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (wasFromBottom) {

            if (player.currentBody != GameObject.Find("Small")){
                player.soundManager.playSound(SoundManager.Sound.BreakBlock);
                GetComponent<SpriteRenderer>().enabled = false;
                transform.Find("break").gameObject.SetActive(true);
                StartCoroutine(destroyAfterTime(timetillbreak));
            } else
            {
                player.soundManager.playSound(SoundManager.Sound.Bump);
            }
        }
    }
    IEnumerator destroyAfterTime(float length) {
        yield return new WaitForSeconds(length);
        GetComponent<SpriteRenderer>().enabled = true;
        transform.Find("break").gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}