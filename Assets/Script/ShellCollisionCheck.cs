﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellCollisionCheck : MonoBehaviour {
    public string side="(type in either 'left' or 'right' w/o ' sign)";

    EnemyController enemyController;

    private void Start() {
        enemyController = transform.parent.parent.GetComponent<EnemyController>();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.tag=="Player") {
            if (!enemyController.unrevivable) {
                switch (side.ToLower()) {
                    case "left":
                        enemyController.moveAsAShell(-1);
                        break;
                    case "right":
                        enemyController.moveAsAShell(1);
                        break;
                }
            }
        }
        if (collision.collider.tag != "ground") {
            enemyController.whenMoveAsAShellChangeDirection();
        }
    }
}