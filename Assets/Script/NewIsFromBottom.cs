﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewIsFromBottom : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.transform.parent.parent.tag == "Player") {
            //            print("was from bottom");
            if (transform.parent.name.Contains("Q"))
                transform.parent.gameObject.GetComponent<QBlock>().wasFromBottom = true;
            else
                transform.parent.gameObject.GetComponent<NewBreakableBlock>().wasFromBottom = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.transform.parent.parent.tag == "Player") {
            if (transform.parent.name.Contains("Q"))
                transform.parent.gameObject.GetComponent<QBlock>().wasFromBottom = true;
            else
                transform.parent.gameObject.GetComponent<NewBreakableBlock>().wasFromBottom = true;
        }
    }
}