﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QBlock : MonoBehaviour {
    public bool isCoin;

    public GameObject coin;
    public GameObject mushroom;
    public GameObject flower;

    public Sprite afterCollision;

    public bool wasFromBottom = false;

    bool enabled = true;

    Vector3 spawnPosition;

    private void Awake() {
        spawnPosition = transform.position + new Vector3(0, .5f, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (enabled) {
            if (wasFromBottom) {
                if (isCoin) {
                    //Debug.Log("Working");
                    Instantiate(coin, spawnPosition, Quaternion.identity);
                } else {
                    switch (GameObject.Find("GameManager").GetComponent<GameController>().powerUpCount) {
                        case 0:
                            //Debug.Log("Working");
                            Instantiate(mushroom, spawnPosition, Quaternion.identity);
                            break;
                        default:
                            //Debug.Log("Working2");
                            Instantiate(flower, spawnPosition, Quaternion.identity);
                            break;
                    }
                }
                GetComponent<Animator>().enabled = false;
                GetComponent<SpriteRenderer>().sprite = afterCollision;
                enabled = false;
            }
        }
    }
}