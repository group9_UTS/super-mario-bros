﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower : MonoBehaviour {
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.gameObject.tag == "Player")
            transform.gameObject.SetActive(false);
    }
}
