﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGrounded : MonoBehaviour {
    NewPlayerController playerController;
    private void Start() {
        playerController = transform.parent.parent.gameObject.GetComponent<NewPlayerController>();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        playerController.SetGrounded(true);
        //collision.gameObject.GetComponent<NewHitBox>().marathon();
        if(collision.collider.tag=="Enemy") {
            collision.gameObject.transform.Find("Goomba").GetComponent<EnemyController>().SetStepped(true);
            GameObject.Find("SoundManager").GetComponent<SoundManager>().playSound(SoundManager.Sound.Stomp);
        }
    }

    private void OnCollisionStay2D(Collision2D collision) {
        playerController.SetGrounded(true);
        //transform.parent.parent.position = new Vector2(transform.parent.parent.position.x, transform.parent.parent.position.y+0.001f);
    }

    private void OnCollisionExit2D(Collision2D collision) {
        playerController.SetGrounded(false);
    }
}
