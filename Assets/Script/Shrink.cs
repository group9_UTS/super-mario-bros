﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrink : StateMachineBehaviour {
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        PlayerController playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        playerController.renewBody();
        playerController.setControllable(true);
        playerController.setAlphaDown();
        playerController.getRigidBody2D().bodyType = RigidbodyType2D.Dynamic;
    }
}