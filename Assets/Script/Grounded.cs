﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script used so players cannot jump in the air
public class Grounded : MonoBehaviour {
    PlayerController player;
    public bool test;
    void Start() {
        //used to call child object of Player
        player = gameObject.transform.parent.GetComponent<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag != "player") {
            test = true;
            player.setGrounded(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.tag != "player") {
            if (collision.tag != "MainCamera") {
                test = true;
                player.setGrounded(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag != "player") {
            test = false;
            player.setGrounded(false);
        }
    }
}