﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2ECollision : MonoBehaviour {
    NewPlayerController player;
    private void Start() {
        player = GameObject.Find("NewPlayer").GetComponent<NewPlayerController>();
    }
    private void OnCollisionEnter2D(Collision2D collision) {
        if (!player.died) {
            if (collision.gameObject.tag == "Enemy") {
                if (player.invincible) collision.gameObject.GetComponentInChildren<EnemyController>().SetStepped(true);
                else player.shrink();
            }
        }
    }
}
