﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    int score;
    float startTime = 400f;
    public float currentTime = 0f;

    public int coinCount;
    public int levelComplete;
    public int enemyCount;
    public int powerUpCount = 3;
    public int extraPowerUpsTaken = 0;

    Text scoreText;
    Text timerText;
    Text coinText;
    Text worldText;

    public int lives;

    Text tempText;

    SoundManager soundManager;

    //PlayerController player;
    NewPlayerController player;

    // Start is called before the first frame update
    private void Awake()
    {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        timerText = GameObject.Find("TimeText").GetComponent<Text>();

        coinText = GameObject.Find("CoinText").GetComponent<Text>();
        worldText = GameObject.Find("WorldText").GetComponent<Text>();

        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        //player = GameObject.Find("Player").GetComponent<NewPlayerController>();
        player = GameObject.Find("NewPlayer").GetComponent<NewPlayerController>();

        currentTime = startTime;
    }

    private void Start() {
        //lives = PlayerPrefs.GetInt("Lives");
        lives = GameObject.Find("SceneController").GetComponent<SceneController>().lifes;

        soundManager.playSound(SoundManager.Sound.BGM);
        StartCoroutine(playLess99());
    }
    IEnumerator playLess99() {
        yield return new WaitForSeconds(300);
        soundManager.playSound(SoundManager.Sound.Less99);
    }

    // Update is called once per frame
    private void Update() {
        normalizePowerupsAndLife();

        currentTime -= 1 * Time.deltaTime;
        timerText.text = "Time\n" + currentTime.ToString("0");

        if (currentTime <= 0)
        {
            currentTime = 0;
            player.Die();
        }

        score = coinCount * 50
        + levelComplete * 100
        + enemyCount * 100
        + powerUpCount * 100
        + extraPowerUpsTaken * 100;

        scoreText.text = ("Mario\n" + score.ToString("D6"));

        coinText.text = "x " + coinCount.ToString("D2");

        worldText.text = "World\n1-" + (levelComplete + 1);
    }

    public void normalizePowerupsAndLife() {
        if (powerUpCount<0) {
            powerUpCount = 0;
            //--lives;
            player.Die();
        }
    }

    public void freezeGame() {
        Time.timeScale = 0;
    }

    public void completedLevel() {
        //do something here
        //GameObject.Find("SceneController").GetComponent<SceneController>().changeScene(1);
        GameObject.Find("SceneController").GetComponent<SceneController>().changeScene(-2);
    }

    public void changescene() {
        SceneController sc = GameObject.Find("SceneController").GetComponent<SceneController>();
        //print(PlayerPrefs.GetInt("Lives"));
        //if (PlayerPrefs.GetInt("Lives") < 1) {
        if (GameObject.Find("SceneController").GetComponent<SceneController>().lifes < 1) {
            sc.changeScene(-2); //change scene to main menu when lives is 0
        } else {
            sc.changeScene(-1); //change scene to loading screen
        }
    }
}