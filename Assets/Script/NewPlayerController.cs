﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerController : MonoBehaviour {
    public float speed;
    public float jumpForce;
    float speedMultiplier;
    public float starLength;

    public Sprite[] sprites;

    public GameObject fire;

    Animator animator;
    SpriteRenderer sRenderer;
    GameController gameController;
    SoundManager soundManager;

    public Sprite Death;

    Rigidbody2D physics;

    bool grounded = true;
    public bool died = false;
    public bool invincible = false;

    int countFire = 0;

    bool isleft = false;

    bool didIDiedInAHole = false;

    // Start is called before the first frame update
    void Start() {
        gameController = GameObject.Find("GameManager").GetComponent<GameController>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        sRenderer = GetComponent<SpriteRenderer>();
        sRenderer.sprite = sprites[gameController.powerUpCount];
        animator = GetComponent<Animator>();
        animator.SetBool("Small",true);
        physics = GetComponent<Rigidbody2D>();
        speedMultiplier = 1f;
    }

    // Update is called once per frame
    void Update() {
        if (!died) {
            //take input
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            //movement
            float xPerFrame = x * Time.deltaTime;
            transform.position = new Vector2(transform.position.x + xPerFrame * speed * speedMultiplier, transform.position.y);

            //animation
            //sprint
            if (Input.GetKey(KeyCode.LeftShift)) {
                animator.SetFloat("animMult", 1.8f);
                speedMultiplier = 1.8f;
            } else {
                animator.SetFloat("animMult", 1.0f);
                speedMultiplier = 1.0f;
            }
            //jump
            if (grounded) {
                animator.SetBool("Jump", false);
                if (Input.GetButtonDown("Jump")) {
                    animator.SetBool("Jump", true);
                    soundManager.playSound(SoundManager.Sound.Jump);
                    physics.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
                }
            }
            //crowch
            if (y < 0) animator.SetBool("Crowch", true);
            else animator.SetBool("Crowch", false);
            //walk
            if (x != 0) {     //if any input
                if (x < 0) {
                    sRenderer.flipX = true;
                    isleft = true;
                } else if (x > 0) {
                    sRenderer.flipX = false;
                    isleft = false;
                }
                animator.SetFloat("Move", Mathf.Abs(x));
            }
            /*
            //test grow
            if (Input.GetKeyDown(KeyCode.Q)) growIfNecessary(gameController.powerUpCount+1);
            //test shrink
            if (Input.GetKeyDown(KeyCode.E)) shrink();
            //test star
            if (Input.GetKeyDown(KeyCode.R)) star();
            */
            //shoot when fire
            if (Input.GetButtonDown("Fire1")) {
                animator.SetTrigger("Shoot");
                if (countFire < 2) {
                    if (gameController.powerUpCount == 2) {
                        Vector2 firespawning = transform.position;
                        int dir;
                        if (isleft) dir = -1;
                        else dir = 1;
                        firespawning = firespawning + new Vector2(dir, 0);
                        GameObject copyOfFire = Instantiate(fire, firespawning, Quaternion.identity, transform);
                        ++countFire;
                        copyOfFire.GetComponent<Rigidbody2D>().AddForce(new Vector2(10*dir,-1), ForceMode2D.Impulse);
                        soundManager.playSound(SoundManager.Sound.Fire);
                    }
                }
            }
        }
    }

    public void releaseCountFire() {
        if (countFire>0) --countFire;
    }

    void growIfNecessary(int newTaken) {
        if (newTaken > gameController.powerUpCount) {
            gameController.powerUpCount= newTaken;
            animator.SetTrigger("Grow");
            soundManager.playSound(SoundManager.Sound.Grow);
            if (gameController.powerUpCount != 0) {
                transform.Find("small").gameObject.SetActive(false);
                transform.Find("big").gameObject.SetActive(true);
            }
        } else {
            gameController.extraPowerUpsTaken++;
        }
    }

    public void shrink() {
        --gameController.powerUpCount;
        animator.SetTrigger("Shrink");
        soundManager.playSound(SoundManager.Sound.Shrink);
        if (gameController.powerUpCount == 0) {
            transform.Find("small").gameObject.SetActive(true);
            transform.Find("big").gameObject.SetActive(false);
        }
        invincible = true;
        StartCoroutine(invincibleFor(1));
    }

    void star() {
        invincible = true;
        StartCoroutine(StarTimer());
        StartCoroutine(ChangeColor());
    }

    public void SetGrounded(bool val) {
        grounded = val;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (!died) {
            switch (collision.transform.gameObject.tag) {
                case "PowerMushroom":
                    growIfNecessary(1);
                    break;
                case "Flower":
                    growIfNecessary(2);
                    break;
                default:
                    break;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        switch (collision.tag) {
            case "Coin":
                ++gameController.coinCount;
                break;
            case "Flag":
                Hold();
                break;
            case "Star":
                star();
                break;
            case "HoleCollider":
                if (!didIDiedInAHole) {
                    collision.GetComponent<BoxCollider2D>().enabled = false;
                    didIDiedInAHole = true;
                    //gameController.lives += 1;
                    Die();
                }
                break;
        }
    }

    public void Die() {
        died = true;
        animator.enabled = false;
        gameController.powerUpCount = 0;
        GameObject.Find("SceneController").GetComponent<SceneController>().lifes = --gameController.lives;
        soundManager.playSound(SoundManager.Sound.Die);
        sRenderer.sprite = Death;
        physics.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        GameObject.DestroyImmediate(transform.GetChild(0).GetComponentInChildren<BoxCollider2D>());
        StartCoroutine(littleInterval());
    }

    public void Hold() {
        animator.SetBool("Jump", false);
        died = true;
        animator.SetBool("Holding", true);
        soundManager.playSound(SoundManager.Sound.Victory);
        physics.velocity = Vector2.zero;
        physics.angularVelocity = 0;
        physics.gravityScale = 0.2f;
        StartCoroutine(chkOnGroundAfterFlag());
    }

    public void setAlphaDown() {
        sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b, .5f);
    }

    IEnumerator littleInterval() {
        yield return new WaitForSeconds(3);
        //PlayerPrefs.SetInt("Lives", gameController.lives);
        gameController.changescene();
    }

    IEnumerator chkOnGroundAfterFlag() {
        while (!grounded) {
            yield return null;
        }
        physics.gravityScale = 1;
        animator.SetBool("Holding", false);
        StartCoroutine(playAfterFlag(2.7f));
    }

    IEnumerator playAfterFlag(float length) {
        //physics.isKinematic = true;
        float starttime = Time.time;
        while (Time.time - starttime < length) {
            transform.position = new Vector2(transform.position.x + 1.5f * Time.deltaTime, transform.position.y);
            yield return null;
        }
        gameController.completedLevel();
    }

    IEnumerator invincibleFor(float length) {
        setAlphaDown();
        yield return new WaitForSeconds(length);
        invincible = false;
        sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b, 1f);
    }

    IEnumerator StarTimer() {
        soundManager.playSound(SoundManager.Sound.Invincible);
        yield return new WaitForSeconds(starLength);
        invincible = false;
        if (gameController.currentTime>99) soundManager.playSound(SoundManager.Sound.BGM);
        else soundManager.playSound(SoundManager.Sound.Less99);
    }

    IEnumerator ChangeColor() {
        float startTime = Time.time;
        float percentageComplete = 0;
        //SpriteRenderer sRenderer = spriteRenderer;
        while (percentageComplete < 1) {
            float elapsedTime = Time.time - startTime;
            percentageComplete = elapsedTime / starLength;
            float a = UnityEngine.Random.Range(0.1f, 1f);
            float b = UnityEngine.Random.Range(0.1f, 1f);
            float c = UnityEngine.Random.Range(0.1f, 1f);
            //if (!sRenderer.Equals(spriteRenderer)) sRenderer = spriteRenderer;
            //sRenderer.color = Color.Lerp(new Color(a,b,c), new Color(b,c,a),percentageComplete);
            //sRenderer.color = new Color(a,b,c);
            //print(spriteRenderer.sprite.name);
            sRenderer.color = new Color(a, b, c);
            yield return null;
        }
        sRenderer.color = Color.white;
    }
}