﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterCrush : StateMachineBehaviour {

    public float length;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.gameObject.GetComponent<EnemyController>().doCoroutine(length);
    }
}