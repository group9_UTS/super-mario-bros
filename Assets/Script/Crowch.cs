﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crowch : StateMachineBehaviour {
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.gameObject.transform.parent.gameObject.GetComponent<PlayerController>().setControllable(false);

    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.gameObject.transform.parent.gameObject.GetComponent<PlayerController>().setControllable(true);
    }
}