﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    //base movement speed
    public float moveSpeed = 5f;
    //used so Mario cant jump more than once
    bool isGrounded = false;

    //used to keep track of player's status
    public static int marioStatus;
    //used to recognize if player is heading left or right
    public bool isLeft = false;

    //used to toggle invincible mode for shrinking down
    bool isStar = false;
    public bool invincible = false;
    public float invincibleLength;
    public float starLength = 10f;
    bool holeDeath = false;

    //toggle to make player uncontrollable (legacy)
    bool isControllable = true;

    //Check if player has fire
    public bool hasFire = false;

    GameController gameController;
    public SoundManager soundManager;
    SpriteRenderer spriteRenderer;
    Animator animator;
    FirePoint firePoint;

    public GameObject currentBody;
    //public GameObject bulletPrefab;
    Color originalColor;

    enum PowerUps {
        Small,
        Super,
        Flower,
        Star
    }

    private void Start() {
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        gameController = GameObject.Find("GameManager").GetComponent<GameController>();
        currentBody = transform.Find("Small").gameObject;
        animator = currentBody.GetComponent<Animator>();
        spriteRenderer = currentBody.GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
        firePoint = GameObject.Find("FirePoint").GetComponent<FirePoint>();
        firePoint.enabled = false;
        marioStatus = 0;
    }

    void Update() {
        //Call sitting function
        //Crowch();
        //Call jump function
        Jump();
        //Move left and right
        Move();
        //call run function
        run();
        if (Input.GetKeyDown(KeyCode.R)) {
            //Shoot();
        }

        if (gameController.currentTime < 0)
        {
            Die();
        }
    }

    private void Crowch() {
        float vertical = Input.GetAxis("Vertical");
        if (vertical < 0) {
            animator.SetBool("Crowch", true);
        } else {
            animator.SetBool("Crowch", false);
        }
    }

    private void Jump() {
        //Mario jump if you press space key and if he is on the ground
        if (Input.GetButtonDown("Jump") && isGrounded) {
            //gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 7f), ForceMode2D.Impulse);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 10f), ForceMode2D.Impulse);
            soundManager.playSound(SoundManager.Sound.Jump);
        }
    }

    private void Move() {
        if (isControllable) {
            float horizontal = Input.GetAxis("Horizontal");

            //for sprite rendering
            if (horizontal < 0) isLeft = true;
            else if (horizontal > 0) isLeft = false;
            
            if (horizontal != 0) animator.SetBool("Walk", true);
            else animator.SetBool("Walk", false);

            spriteRenderer.flipX = isLeft;

            //movement logics
            Vector3 movement = new Vector3(horizontal, 0f, 0f);
            transform.position += movement * Time.deltaTime * moveSpeed;
        }
    }

    private void run() {
        //hold shift key to run
        if (Input.GetKey(KeyCode.LeftShift)) {
            moveSpeed = 8f;
            animator.SetFloat("speed",1.7f);
        }
        else {
            moveSpeed = 5f;
            animator.SetFloat("speed", 1.0f);
        }
    }

    public void Die()
    {
        PlayerPrefs.SetInt("Lives", --gameController.lives);
        soundManager.playSound(SoundManager.Sound.Die);
        StartCoroutine(DiePause());

    }

    IEnumerator DiePause()
    {
        float waitCount = 0f;
        gameController.freezeGame();
        while (waitCount < 5f)
        {
            waitCount += Time.unscaledDeltaTime;

            yield return 0;
        }
        Time.timeScale = 1;
        ChangeScene();
    }

    void Win()
    {
        soundManager.playSound(SoundManager.Sound.Victory);
        StartCoroutine(WinPause());
    }

    IEnumerator WinPause()
    {
        float waitCount = 0f;
        gameController.freezeGame();
        while (waitCount < 7f)
        {
            waitCount += Time.unscaledDeltaTime;

            yield return 0;
        }
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }

    void ChangeScene()
    {
        if (PlayerPrefs.GetInt("Lives") < 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2); //change scene to main menu when lives is 0
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1); //change scene to loading screen
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        switch(collision.collider.tag) {
            case "Enemy":
                if (invincible != true) {
                    if (gameController.powerUpCount < 1) {
                        Die();
                    } else {
                        if (hasFire == true)
                        {
                            hasFire = false;
                            firePoint.enabled = false;
                            powerDown(gameController.powerUpCount--);
                        } else {
                            powerDown(gameController.powerUpCount--);
                        }
                    }
                } else {
                    Destroy(collision.gameObject);
                    ++gameController.enemyCount;
                    soundManager.playSound(SoundManager.Sound.Stomp);
                }
                break;
            case "EnemyStepCheck":
                getRigidBody2D().AddForce(new Vector2(0,5f),ForceMode2D.Impulse);
                break;
            case "Flag":
                Win();
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        switch (collision.tag) {
            case "Coin":
                //Debug.Log("working");
                Destroy(collision.gameObject);
                gameController.coinCount += 1;
                collision.gameObject.SetActive(false);
                soundManager.playSound(SoundManager.Sound.Coin);
                break;
            case "PowerMushroom":
                powerUp(PowerUps.Super);
                collision.gameObject.SetActive(false);
                break;
            case "Star":
                powerUp(PowerUps.Star);
                collision.gameObject.SetActive(false);
                break;
            case "Flower":
                powerUp(PowerUps.Flower);
                collision.gameObject.SetActive(false);
                break;
            case "1Up":
                ++gameController.lives;
                Destroy(collision.gameObject);
                collision.gameObject.SetActive(false);
                break;
            case "HoleCollider":
                if (holeDeath != true)
                {
                    holeDeath = true;
                    Die();
                }
                break;
        }
    }

    private void disablePlayerControl() {
        isControllable = false;
        getRigidBody2D().bodyType = RigidbodyType2D.Static;
        animator.SetBool("Jump", false);
    }

    private void powerUp(PowerUps powerUps) {
        if (powerUps != PowerUps.Star) {
            if ((int)powerUps > gameController.powerUpCount) {
                disablePlayerControl();
                gameController.powerUpCount = (int)powerUps;
                animator.SetTrigger("Grow");
                soundManager.playSound(SoundManager.Sound.Grow);
            }
        }
        switch (powerUps) {
            case PowerUps.Flower:
                FirePower();
                soundManager.playSound(SoundManager.Sound.Grow);
                break;
            case PowerUps.Star:
                StarPower();
                isStar = true;
                soundManager.playSound(SoundManager.Sound.Invincible);
                soundManager.playSound(SoundManager.Sound.Grow);
                break;
        }
    }
    private void powerDown(int whatwasbefore) {
        isControllable = false;
        invincible = true;
        StartCoroutine(invincibleFor(invincibleLength));
        getRigidBody2D().bodyType = RigidbodyType2D.Static;
        animator.SetBool("Jump", false);
        animator.SetTrigger("Shrink");
        soundManager.playSound(SoundManager.Sound.Shrink);
    }

    public void renewBody() {
        currentBody.SetActive(false);
        currentBody = transform.Find(getSizeName(gameController.powerUpCount)).gameObject;
        currentBody.SetActive(true);
        spriteRenderer = currentBody.GetComponent<SpriteRenderer>();
        animator = currentBody.GetComponent<Animator>();
    }

    void StarPower() {
        invincible = true;
        StartCoroutine(StarTimer());
        StartCoroutine(ChangeColor());
    }

    void FirePower()
    {
        if (hasFire != true && !firePoint.enabled)
        {
            hasFire = true;
            firePoint.enabled = true;
        }
    }

    IEnumerator StarTimer() {
        yield return new WaitForSeconds(starLength);
        invincible = false;
        isStar = false;
        spriteRenderer.color = originalColor;
        soundManager.playSound(SoundManager.Sound.BGM);
    }

    IEnumerator ChangeColor() {
        float startTime = Time.time;
        float percentageComplete = 0;
        //SpriteRenderer sRenderer = spriteRenderer;
        while (percentageComplete < 1) {
            float elapsedTime = Time.time - startTime;
            percentageComplete = elapsedTime/starLength;
            float a = UnityEngine.Random.Range(0.1f, 1f);
            float b = UnityEngine.Random.Range(0.1f, 1f);
            float c = UnityEngine.Random.Range(0.1f, 1f);
            //if (!sRenderer.Equals(spriteRenderer)) sRenderer = spriteRenderer;
            //sRenderer.color = Color.Lerp(new Color(a,b,c), new Color(b,c,a),percentageComplete);
            //sRenderer.color = new Color(a,b,c);
            //print(spriteRenderer.sprite.name);
            spriteRenderer.color = new Color(a,b,c);
            yield return null;
        }
    }

    public void setGrounded(bool status) {
        isGrounded = status;
        animator.SetBool("Jump", !status);
    }

    public void setControllable(bool status) {
        isControllable = status;
    }

    private string getSizeName(int size) {
        switch (size) {
            case 0:
                return "Small";
            case 1:
                return "Big";
            case 2:
                return "Fire";
        }
        return "";
    }

    public Rigidbody2D getRigidBody2D() {
        return gameObject.GetComponent<Rigidbody2D>();
    }

    IEnumerator invincibleFor(float length) {
        yield return new WaitForSeconds(length);
        invincible = false;
        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 1f);
    }

    public void setAlphaDown() {
        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, .5f);
    }
}