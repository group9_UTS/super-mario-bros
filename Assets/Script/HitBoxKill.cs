﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxKill : MonoBehaviour
{

    public AudioSource Hit;
    
    GameObject Enemy;

    GameController gameManager;
    SoundManager soundManager;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameController>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        Enemy = gameObject.transform.parent.parent.Find("Goomba").gameObject;
        
    }

  

    private void OnTriggerEnter2D(Collider2D collision)
    {
       //kill enemy when jump on top of it
        if (collision.gameObject.tag.Equals("PlayerHitBox"))
        {
            //play a sound effect
            //Hit.Play();
            //make the gameobject disapear
            //Destroy(Enemy.gameObject);
            EnemyController enemyController = Enemy.GetComponent<EnemyController>();
            enemyController.SetStepped(true);
            enemyController.doCoroutine(1);
            ++gameManager.enemyCount;
            soundManager.playSound(SoundManager.Sound.Stomp);
        }

    }
}
