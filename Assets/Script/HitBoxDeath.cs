﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxDeath : MonoBehaviour
{
    public AudioSource Lose;
    public AudioSource Background;

    
   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //detects when players hit an enemy
        if (collision.gameObject.tag.Equals("Player"))
        {
            Background.Stop();
            //lose health (atm it only play an audio)
            Lose.Play();
        }

    }
}
