﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterRegen : StateMachineBehaviour {
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        GameObject target = animator.gameObject;
        target.GetComponent<EnemyController>().SetStepped(false);
        target.transform.Find("InShellCollisionBox").gameObject.SetActive(false);
        target.transform.Find("PlayerStep").GetComponent<PolygonCollider2D>().enabled = true;
        target.GetComponent<PolygonCollider2D>().enabled = true;
        animator.SetBool("Stepped", false);
    }
}