﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewFire : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(killself());
    }

    IEnumerator killself() {
        yield return new WaitForSeconds(2.5f);
        transform.parent.GetComponent<NewPlayerController>().releaseCountFire();
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.tag == "Enemy") {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            collision.gameObject.GetComponentInChildren<EnemyController>().SetStepped(true);
            transform.parent.GetComponent<NewPlayerController>().releaseCountFire();
            Destroy(gameObject);
        }
    }
}