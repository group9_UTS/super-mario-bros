﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBreakableBlock : MonoBehaviour {
    public bool wasFromBottom = false;
    public float timetillbreak;
    GameController gamecontroller;
    SoundManager soundManager;

    private void Start() {
        gamecontroller = GameObject.Find("GameManager").GetComponent<GameController>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (wasFromBottom) {
            if (gamecontroller.powerUpCount!=0) {
                soundManager.playSound(SoundManager.Sound.BreakBlock);
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                transform.Find("break").gameObject.SetActive(true);
                StartCoroutine(destroyAfterTime(timetillbreak));
            } else {
                soundManager.playSound(SoundManager.Sound.Bump);
            }
        }
    }
    IEnumerator destroyAfterTime(float length) {
        yield return new WaitForSeconds(length);
        GetComponent<SpriteRenderer>().enabled = true;
        transform.Find("break").gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}