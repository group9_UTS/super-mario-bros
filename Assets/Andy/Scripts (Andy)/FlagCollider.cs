﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlagCollider : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Small" || collision.gameObject.name == "Big" || collision.gameObject.name == "Fire") //Search for player gameobject tagged "Player"
        {
            Debug.Log("PLAYER WIN");
            StartCoroutine(WinPause()); //Pause scene for 5 seconds upon win, should replace with level completion animation
            SceneManager.LoadScene(0); //Loads back to Main Menu scene (scene0)
        }
    }

    IEnumerator WinPause()
    {
        yield return new WaitForSeconds(5);
    }
}
