﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingLife : MonoBehaviour {
    GameController gameController;

    public Text livesText;

    // Start is called before the first frame update
    void Start() {
        Time.timeScale = 1;
        //livesText.text = PlayerPrefs.GetInt("Lives").ToString();
        livesText.text = GameObject.Find("SceneController").GetComponent<SceneController>().lifes.ToString();
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene() {
        StopCoroutine(LoadScene());
        yield return new WaitForSeconds(4f);
        GameObject.Find("SceneController").GetComponent<SceneController>().changeScene(1);
    }
}