﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HoleCollider : MonoBehaviour
{
    public SoundManager soundManager;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") //Search for player gameobject tagged "Player"
        {
            Debug.Log("PLAYER LOSE");
            StartCoroutine(LosePause()); //Pause scene for 3 seconds upon death, should replace with death animation
        }
    }

    IEnumerator LosePause()
    {
        yield return new WaitForSeconds(2);
        soundManager.playSound(SoundManager.Sound.Die);
        SceneManager.LoadScene(1); //Loads back to LoadingLife scene (scene1) with UPDATED LIFE COUNTER
    }
}
