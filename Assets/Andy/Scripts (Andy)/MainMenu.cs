﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void StartGame() {
        GameObject.Find("SceneController").GetComponent<SceneController>().changeScene(1);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
