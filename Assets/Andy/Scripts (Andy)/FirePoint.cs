﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePoint : MonoBehaviour
{
    public GameObject bulletPrefab;
    public bool canShoot = true;

    private int count = 0;

    SoundManager soundManager;
    PlayerController playerController;

    private void Start()
    {
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && canShoot && count < 3)
        {
            Shoot();
            count++;
            if (count == 3)
            {
                StartCoroutine(CanShoot());
            }
        }
    }

    void Shoot()
    {
        GameObject b = Instantiate(bulletPrefab) as GameObject;
        if (playerController.isLeft) b.transform.position = gameObject.transform.position + Vector3.left;
        else b.transform.position = gameObject.transform.position + Vector3.right;
        soundManager.playSound(SoundManager.Sound.Fire);
    }

    IEnumerator CanShoot()
    {
        canShoot = false;
        yield return new WaitForSeconds(1);
        count = 0;
        canShoot = true;
    }
}
